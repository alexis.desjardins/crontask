from datetime import datetime
from airflow.decorators import dag, task


@dag(schedule_interval='*/5 * * * *', start_date=datetime(2021, 1, 1), catchup=False, tags=['example'])
def genericFirstDagRepo():
    @task.virtualenv(
        task_id="getFromApi",
        requirements=['python-dotenv==0.19.2', 'requests==2.27.1'],
        system_site_packages=False
    )
    def collect():
        return [123, 4959, 2993, 2900, 2839, 9329228, 28827, 27738, 81276, 29200]

    @task()
    def processing(total_order_value: [float]):
        return sum(total_order_value) / len(total_order_value)

    @task()
    def load(total_order_value: float):
        print(f"Total value is: {total_order_value:.2f}")

    data = collect()
    processed = processing(data)
    load(processed)


genericFirstDagRepo = genericFirstDagRepo()
